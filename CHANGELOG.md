# ChangeLog

## Unreleased

## 1.1.1 (2021-06-05)

**Changed:**
* Lib dependencies

## 1.1.0 (2021-01-28)

**Changed:**
* Switched to Vert.x 4.0.0

## 1.0.4 (2020-06-18)

**Changed:**
* Pipe startTime

## 1.0.3 (2020-05-28)

**Fixed:**
* Send original id

## 1.0.2 (2020-05-28)

**Fixed:**
* Send id list after last dataset sent

## 1.0.1 (2020-05-27)

**Fixed:**
* Forwarding bad pipe

## 1.0.0 (2020-05-27)

Initial release

**Added:**
* Quality check job in gitlab ci

**Fixed:**
* Log output at the end of importing
